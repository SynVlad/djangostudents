from django.urls import path
from student import views


urlpatterns = [
    path('list-of-students/', views.AllStudentListView.as_view(), name='list_of_students'),
    path('create-student/', views.StudentCreateView.as_view(), name='create_student'),
    path('update-student/<int:pk>/', views.StudentUpdateView.as_view(), name='update_student'),
    path('delete-student/<int:pk>/', views.StudentDeleteView.as_view(), name='delete_student'),
    path('details-student/<int:pk>', views.StudentDetailsView.as_view(), name='details_student'),
    path('inactive-student/<int:pk>/', views.inactive_student, name='inactive_student'),
    path('delete-student-with-modal/<int:pk>', views.delete_student_with_modals, name='delete_student_with_modal'),
    path('all-students-per-teacher/<int:id_teacher>', views.get_students_for_teacher, name='all_students_per_teacher')
]
