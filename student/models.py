from django.db import models
from teacher.models import Teacher


class Student(models.Model):
    gender_option = (('male', 'Male'), ('female', 'Female'), ('other', 'Other'))

    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    age = models.IntegerField()
    is_olympic = models.BooleanField(default=False)
    email = models.EmailField(max_length=200)
    description = models.TextField(max_length=400)
    start_date_time = models.DateTimeField()
    end_date = models.DateField()
    gender = models.CharField(choices=gender_option, max_length=6)
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)

    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'
