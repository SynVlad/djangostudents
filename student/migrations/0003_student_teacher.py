# Generated by Django 4.0.3 on 2022-04-17 10:52

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('teacher', '0001_initial'),
        ('student', '0002_rename_end_date_time_student_end_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='teacher',
            field=models.ForeignKey(default=2, on_delete=django.db.models.deletion.CASCADE, to='teacher.teacher'),
            preserve_default=False,
        ),
    ]
