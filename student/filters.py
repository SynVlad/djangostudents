import django_filters
from django import forms

from student.models import Student


class StudentFilter(django_filters.FilterSet):
    first_name = django_filters.CharFilter(lookup_expr='icontains', label='First Name')
    last_name = django_filters.CharFilter(lookup_expr='icontains', label='Last Name')
    created_at__gte = django_filters.DateTimeFilter(field_name='created_at', lookup_expr='gte', label='From',
                                                    widget=forms.DateInput(attrs={'class': 'form-control',
                                                                                  'type': 'date'}))
    created_at__lte = django_filters.DateTimeFilter(field_name='created_at', lookup_expr='lte', label='To',
                                                    widget=forms.DateInput(attrs={'class': 'form-control',
                                                                                  'type': 'date'})
                                                    )

    class Meta:
        model = Student
        fields = ['first_name', 'last_name', 'gender', 'is_olympic', 'created_at__gte', 'created_at__lte']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.filters['first_name'].field.widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Search for first name'})
        self.filters['last_name'].field.widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Search for last name'})
        self.filters['gender'].field.widget.attrs.update({'class': 'form-select'})
        self.filters['is_olympic'].field.widget.attrs.update({'class': 'form-select'})
