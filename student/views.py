from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView

from student.filters import StudentFilter
from student.forms import StudentForm
from student.models import Student
from teacher.models import Teacher


def first(request):
    return HttpResponse('Student test')

#
# class StudentTemplateView(TemplateView):
#     template_name = 'student/student.html'



class StudentCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'student/create-student.html'
    model = Student
    # fields = '__all__' get all student fields from student models in model order
    form_class = StudentForm
    success_url = reverse_lazy('list_of_students')
    permission_required = 'student.add_student'
        
        
class AllStudentListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'student/students_list.html'
    model = Student
    context_object_name = 'all_students'
    permission_required = 'student.view_student'
    
    def get_queryset(self):
        return Student.objects.filter(active=True)

    def get_context_data(self, *args, **kwargs):
        data = super().get_context_data(**kwargs)
        students = Student.objects.all()
        student_filters = StudentFilter(self.request.GET, queryset=students)
        students = student_filters.qs
        data['all_students'] = students
        data['students_filters'] = student_filters

        return data


class StudentUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'student/update_student.html'
    model = Student
    form_class = StudentForm
    # success_url = reverse_lazy('list_of_students')
    permission_required = 'student.change_student'
    
    def get_success_url(self):
        student_id = self.object.id 
        return reverse('update_student', args=[student_id])
    
    
class StudentDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'student/delete-student.html'
    model = Student
    success_url = reverse_lazy('list_of_students')
    permission_required = 'student.delete_student'


class StudentDetailsView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = 'student/detail-student.html'
    model = Student
    permission_required = 'student.details_student'


@login_required()
@permission_required('student.inactive_student')
def inactive_student(request, pk):
    Student.objects.filter(id=pk).update(active=False)
    return redirect('list_of_students')




@login_required()
@permission_required('student.delete_student')
def delete_student_with_modals(request, pk):
    Student.objects.filter(id=pk).delete()
    return redirect('list_of_students')


@login_required()
@permission_required('student.students_per_teacher')
def get_students_for_teacher(request, id_teacher):
    teacher = Teacher.objects.get(id=id_teacher)
    all_students = Student.objects.filter(teacher_id=id_teacher)
    context = {'get_students_per_teacher': all_students, 'teacher': teacher}
    return render(request, 'student/student_per_teacher.html', context)
