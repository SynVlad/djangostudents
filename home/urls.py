from django.urls import path
from home import views


urlpatterns = [
    path('all-cars/', views.cars, name='all_cars'),
    path('apps/', views.applications, name='apps'),
    path('', views.HomeTemplateView.as_view(), name='home')
]
