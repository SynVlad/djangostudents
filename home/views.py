from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView


def index(request):
    return HttpResponse('<h1>Hello world</h1')


def cars(request):
    context = {
        'all_cars': [
            {
                'name': 'Tesla',
                'model': 'Model Y',
                'color': 'blue',
                'options': ['auto-pilot', 'seat heating']
            },
            {
                'name': 'Maybach',
                'model': '62S',
                'color': 'black',
                'options': ['seat heating', 'breaks', 'windscreen', 'whell']
            }
        ]
    }
    return render(request, 'home/all_cars.html', context)


'''
1. O functie prin care veti defini 3 produse din categoria respectiva
2. veti adauga un url nou in urls.py din aplicatia home
3. veti adauga un fisier nou .html unde veti afisa informatiile despre cele 3 produse
'''


def applications(request):
    context = {
        'all_aps': [
            {
                'name': 'Blog',
                'story': 'A blog about philosophy',
                'software': ['Django', 'Bootstrap', 'FastAPI', 'MongoDB'],
                'proprieties': {
                    'background_color': 'red',
                    'text_size': '14px',
                    'padding': '2px'
                }
            },
            {
                'name': 'Portfolio',
                'story': 'A portofolio about me',
                'software': ['Django', 'Bootstrap', 'FastAPI', 'MongoDB'],
                'proprieties': {
                    'background_color': 'red',
                    'text_size': '14px',
                    'padding': '2px'
                }
            },
            {
                'name': 'Learning',
                'story': 'A learning site about some language',
                'software': ['Django', 'Bootstrap', 'FastAPI', 'MongoDB'],
                'proprieties': {
                    'background_color': 'red',
                    'text_size': '14px',
                    'padding': '2px'
                }
            }
        ]
    }
    return render(request, 'home/apps.html', context)


class HomeTemplateView(TemplateView):
    template_name = 'home/home.html'
