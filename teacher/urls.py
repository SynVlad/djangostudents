from django.urls import path
from teacher import views


urlpatterns = [
    path('create-teacher/', views.TeacherCreateViews.as_view(), name='create_teacher'),
    path('list-of-teachers/', views.AllTeacherListView.as_view(), name='list_of_teachers'),
    path('update-teacher/<int:pk>/', views.UpdateTeacherView.as_view(), name='update_teacher'),
    path('delete-teacher/<int:pk>/', views.DeleteTeacherView.as_view(), name='delete_teacher'),
    path('inactive-teacher/<int:pk>/', views.inactive_teacher, name='inactive_teacher')
]
