from django.db import models


class Teacher(models.Model):
    course_option = (('python', 'Python'), ('javascript', 'JavaScript'),
                     ('java', 'Java'))

    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    department = models.CharField(max_length=40)
    course = models.CharField(choices=course_option, max_length=10)

    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name} - {self.course}'
