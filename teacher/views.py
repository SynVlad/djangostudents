from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import redirect
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView, ListView, UpdateView, DeleteView

from teacher.filter import TeacherFilter
from teacher.forms import TeacherForm
from teacher.models import Teacher


class TeacherCreateViews(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'teacher/create-teacher.html'
    model = Teacher
    form_class = TeacherForm
    success_url = reverse_lazy('list_of_teachers')
    permission_required = 'teacher.add_teacher'


class AllTeacherListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'teacher/list_of_teachers.html'
    model = Teacher
    context_object_name = 'all_teachers'
    permission_required = 'teacher.view_teacher'

    def get_queryset(self):
        # return Teacher.objects.filter(course='python')
        return Teacher.objects.filter(active=True)

    def get_context_data(self, *args, **kwargs):
        data = super().get_context_data(**kwargs)
        teachers = Teacher.objects.all()
        teachers_filter = TeacherFilter(self.request.GET, queryset=teachers)
        teachers = teachers_filter.qs
        data['all_teachers'] = teachers
        data['teachers_filter'] = teachers_filter

        return data


class UpdateTeacherView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'teacher/update_teacher.html'
    model = Teacher
    form_class = TeacherForm
    permission_required = 'teacher.change_teacher'

    def get_success_url(self):
        teacher_id = self.object.id
        return reverse('update_teacher', args=[teacher_id])


class DeleteTeacherView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'teacher/delete_teacher.html'
    model = Teacher
    success_url = reverse_lazy('list_of_teachers')
    permission_required = 'teacher.delete_teacher'


@login_required()
@permission_required('teacher.inactive_teacher')
def inactive_teacher(request, pk):
    Teacher.objects.filter(id=pk).update(active=False)
    return redirect('list_of_teachers')
