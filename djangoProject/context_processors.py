from teacher.models import Teacher


def get_all_teachers(request):
    all_teachers = Teacher.objects.filter(active=True)
    return {'get_all_teacher': all_teachers}
