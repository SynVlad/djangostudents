from django.contrib.auth.models import User
from django.db import models


class UserExtend(User):
    city_options = (('iasi', 'Iasi'), ('cluj', 'Cluj'), ('arad', 'Arad'))
    gender_options = (('male', 'Male'), ('female', 'Female'), ('other', 'Other'))

    phone = models.IntegerField()
    address = models.CharField(max_length=200)
    city = models.CharField(choices=city_options, max_length=4)
    gender = models.CharField(choices=gender_options, max_length=6)

    def __str__(self):
        return f'{self.username}'
